<?php
namespace AnzahTools\LastestActivityWidget\Widget;

use XF\Widget\AbstractWidget;
use XF\Mvc\Entity\ArrayCollection;
use XF\Mvc\ParameterBag;

/**
 * Class LastOnline
 *
 * @package AnzahTools\LastMemberOnline\Widget
 */
class LastestActivity extends AbstractWidget
{
	protected $defaultOptions = [
		'at_law_maxItems' => '5',
		'at_law_more' => 'none'
	];

	public function render()
	{
		$options = $this->options;
		$at_law_maxItems = $options['at_law_maxItems'];

		$newsFeedRepo = $this->repository('XF:NewsFeed');

		$newsFeedFinder = $newsFeedRepo->findNewsFeed()
			->beforeFeedId($beforeId);

		$items = $newsFeedFinder->fetch($at_law_maxItems * 2);
		$newsFeedRepo->addContentToNewsFeedItems($items);
		$items = $items->filterViewable();

		/** @var ArrayCollection $items */
		$items = $items->slice(0, $at_law_maxItems);

		$lastItem = $items->last();
		$oldestItemId = $lastItem ? $lastItem->news_feed_id : 0;

		$viewParams = [
			'newsFeedItems' => $items,
			'oldestItemId' => $oldestItemId,
		];
		return $this->renderer('at_law_widget',$viewParams);
	}

	public function verifyOptions(\XF\Http\Request $request, array &$options, &$error = null)
	{
		$options = $request->filter([
			'at_law_maxItems' => 'str',
			'at_law_more' => 'str'
		]);
		return true;
	}
}
